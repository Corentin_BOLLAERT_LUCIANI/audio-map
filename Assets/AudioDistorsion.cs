using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDistorsion : MonoBehaviour
{
    public List<AudioSource> AudiosToDecrease;
    public List<AudioSource> AudiosToIncrease;

    //public float startTime;
    public float timeToComplete;

    private bool start = false;

    private Dictionary<AudioSource, float> AudiosInitialParameters = new Dictionary<AudioSource, float>();

    private void Start()
    {
        foreach (AudioSource item in AudiosToDecrease)
        {
            AudiosInitialParameters.Add(item, item.volume);
         };
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            start = true;
        }

        if (start)
        {
            foreach (KeyValuePair<AudioSource, float> item in AudiosInitialParameters)
            {
                item.Key.volume -= item.Value / (timeToComplete / Time.deltaTime);
            }

            foreach ( AudioSource item in AudiosToIncrease)
            {
                item.maxDistance += 10 / (timeToComplete / Time.deltaTime);
            }
        }
    }

}
