using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    public Light myLight;

    // Update is called once per frame
    void Update()
    {
        if(Random.value < .5) myLight.enabled = !myLight.enabled;

    }
}
